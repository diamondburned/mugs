package main

import (
	"errors"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

/*
A walkthrough of the schema

1. time.Time is fast forwarded with a constant

*/

// Cache is a json file containing uuids and their expiry date
type Cache struct {
	*os.File
	Path string
	Lock sync.RWMutex
}

const (
	// FallbackTmp is the directory to fallback when can't get the tmp variable
	FallbackTmp string = "/tmp/mugs"

	// CacheName is the name of the cache database
	CacheName string = "cache.json"
)

var (
	// CleanFrequency ..
	CleanFrequency = time.Duration(time.Minute * 2)

	// FileAge is the age of the cache opus file
	FileAge = time.Duration(time.Hour * 5)

	// ErrExpiryDNE is returned when the database fails to find the UUID
	ErrExpiryDNE = errors.New("Expiry date does not exist for UUID")
)

// Clean cleans the database of outdated entries
func Clean() {
	err := filepath.Walk("/tmp/mugs/", func(path string, info os.FileInfo, err error) error {
		/*
			1. Get current time
			2. Subtract ^ to the FileAge to get the time it should've been created
			if it were to be expired
			3. Get the file time and subtract that to the time we got above
			4. If a negative is returned, it's expired (I think)
		*/
		if info.ModTime().Sub(time.Now().Add(-FileAge)) < 0 {
			return os.Remove(path)
		}

		return nil
	})

	if err != nil {
		log.Println(err)
	}
}

func getTmpDir() string {
	tmp := os.Getenv("XDG_CACHE_HOME")
	if tmp == "" {
		return FallbackTmp
	}

	return tmp
}
