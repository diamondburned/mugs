package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
)

func serve(r chi.Router) {
	r.Get("/{uuid}", func(w http.ResponseWriter, r *http.Request) {
		uuid := chi.URLParam(r, "uuid")

		if ent, ok := GetEntityFromUUID(uuid); ok {
			path := fmt.Sprintf("/tmp/mugs/%s.opus", ent.UUID)
			prCh := make(chan FFMpegProgress)

			file, err := os.OpenFile(path, os.O_RDONLY|os.O_SYNC, os.ModePerm)
			if err != nil {
				go ent.Encode(path, prCh)
				<-prCh // wait for ffmpeg to actually start
			} else {
				close(prCh)
			}

			w.Header().Set("Content-Type", "audio/ogg")

			if file == nil {
				file, err = os.OpenFile(path, os.O_RDONLY|os.O_SYNC, os.ModePerm)
				if err != nil {
					http.Error(w, err.Error(), 500)
					return
				}
			}

			if _, err := copyBuffer(w, file, prCh); err != nil {
				log.Println(err)
				http.Error(w, err.Error(), 500)
				return
			}

			//io.Copy(w, file)
		} else {
			http.Error(w, "Not found", 404)
			return
		}

		// ctx := r.Context()
		// article, ok := ctx.Value("article").(*Article)
		// if !ok {
		// 	http.Error(w, http.StatusText(422), 422)
		// 	return
		// }
		// w.Write([]byte(fmt.Sprintf("title:%s", article.Title)))
	})
}
