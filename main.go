package main

import (
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	listening = ":3000"
)

var (
	entities Entities
	walkWG   sync.WaitGroup

	r = chi.NewRouter()

	// Root is the path of crawling
	Root = "/home/diamond/Music/Library"
)

// Entities contain maps
type Entities []*Entity

func init() {
	if len(os.Args) > 1 {
		println(os.Args[1])
		Root = os.Args[1]
	}

	// entities.Map = make(map[string]*Entity)
	// entities.lock = &sync.RWMutex{}

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Use(middleware.Timeout(time.Hour))
}

func main() {
	log.SetFlags(log.Lshortfile)

	if err := os.MkdirAll("/tmp/mugs/", 0664); err != nil {
		log.Panicln(err)
	}

	fs := http.FileServer(http.Dir("./public"))
	r.Get("/*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))

	r.Route("/serve", serve)
	r.Route("/api", api)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()

		log.Fatalln(http.ListenAndServe(listening, r))
	}()

	Walk()
	walkWG.Wait()

	// for _, entity := range entities.Map {
	// 	prCh := make(chan FFMpegProgress)
	// 	path := fmt.Sprintf("/tmp/%s.opus", entity.UUID)

	// 	go entity.Encode(path, prCh)

	// 	for prog := range prCh {
	// 		spew.Dump(prog)

	// 		if prog.Progress == ProgressEnd {
	// 			break
	// 		}
	// 	}

	// 	break
	// }

	wg.Wait()

	os.Exit(0)
}
